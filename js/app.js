/*
# Memory card
# Originally a little game made in class
# Modified by NaorimSenchai
#
# You can copy and edit this script without asking for any permission.
# Just don't claim it as your own if you repost it.
# Or else I'll bite you.
# Very hard.
*/

const cartes = document.querySelectorAll('.carte');
let carteRetournee = false;
let premiereCarte, secondeCarte;
let verrouillage = false;
let healthIcons = document.querySelectorAll('.health');
let health = healthIcons.length;
let points = 0;
let cartesRestantes = 30;

cartes.forEach(carte => {
	carte.addEventListener("click", retourneCarte);
});

// Le bouton de rechargement de la page (kan té mor)
document.querySelectorAll('.reloadButton').forEach(btn =>{
	btn.addEventListener("click", ()=>{
		location.reload();
	});
});

function retourneCarte(){
	if (!verrouillage){
		// console.log(this);
		// console.log(this.childNodes[1]);
		if (!this.childNodes[1].classList.contains("active")){
			playAudio("turn.mp3");
			this.childNodes[1].classList.toggle('active');

			if (!carteRetournee){
				carteRetournee = true;
				premiereCarte = this;
				return;
			};
			verrouillage = true;
			carteRetournee = false;
			secondeCarte = this;

			// console.log(premiereCarte, secondeCarte);
			correspondance();
		};
	};
};

function correspondance(){
	if (premiereCarte.getAttribute('data-attr') === secondeCarte.getAttribute('data-attr')){
		setTimeout(() => {
			premiereCarte.removeEventListener('click', retourneCarte);
			secondeCarte.removeEventListener('click', retourneCarte);
			playAudio("good.mp3");
			secondeCarte.classList.add(".valid");
			if (health < healthIcons.length){
				// healthIcons[health].classList.add('visible');
				healthIcons[health].src = "img/heart.svg";
				health += 1;
			};
			points += 10;
			updateScore();
			verrouillage = false;
			cartesRestantes -= 2;

			if (cartesRestantes === 0){
				gameWin();
			};
		}, 1000);
	} else{
		setTimeout(() => {
			premiereCarte.childNodes[1].classList.remove('active');
			secondeCarte.childNodes[1].classList.remove('active');
			healthIcons[health - 1].src = "img/emptyHeart.svg";

			health -= 1;
			if (health > 0){
				playAudio("bad.mp3");
				if (points > 0){
					points -= 1;
				};
				verrouillage = false;
			} else{
				if (points > 0){
					points -= 1;
				};
				gameOver();
			};
			updateScore();
		}, 1000);
	};
};

function aleatoire(){
	cartes.forEach(card =>{
		let randomPos = Math.floor(Math.random() * 12);
		card.style.order = randomPos;
	});
};

aleatoire();

function playAudio(_sound){
	/*
	Function playing a sound.

	ex:
	playAudio("sound.ogg");
	*/
	let sound = new Audio(`audio/${_sound}`);
	sound.play();
};

function updateScore(){
	document.getElementById('score').innerText = points;
};

function gameOver(){
	playAudio("gameOver.mp3");
	document.getElementById("gameOver").style.visibility = "visible";
};

function gameWin(){
	playAudio("victory.mp3");
	document.getElementById("gameWin").style.visibility = "visible";
};

document.addEventListener('click', ()=>{
	console.log(window.innerHeight);
	console.log(window.innerWidth);
});

document.querySelectorAll('.grille').forEach(grille =>{
	document.addEventListener("resize", ()=>{
		location.reload();
	});
});